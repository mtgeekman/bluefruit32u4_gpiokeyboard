/*Bit 0 (0x01): Left Control
Bit 1 (0x02): Left Shift
Bit 2 (0x04): Left Alt
Bit 3 (0x08): Left Window
Bit 4 (0x10): Right Control
Bit 5 (0x20): Right Shift
Bit 6 (0x40): Right Alt
Bit 7 (0x80): Right Window*/

#define BIT_LEFT_CONTROL      0x01
#define BIT_LEFT_SHIFT        0x02
#define BIT_LEFT_ALT          0x04
#define BIT_LEFT_CONTROL_ALT  0x05
#define BIT_LEFT_WINDOW       0x08
#define BIT_RIGHT_CONTROL     0x10
#define BIT_RIGHT_SHIFT       0x20
#define BIT_RIGHT_ALT         0x40
#define BIT_RIGHT_WINDOW      0x80
